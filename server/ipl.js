/* eslint-disable no-undef */
/* eslint-disable eqeqeq */
/* eslint-disable max-len */
/* eslint-disable no-shadow */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable radix */
// Function 1. By using Forloop.

// 1. Number of matches played per year for all the years in IPL.

/* function matchPlayed(matchJsonFile){
    var totalfile=matchJsonFile.length;
    var obj={};
    var counter=0;
    for(let i=0;i<totalfile;i++)
    {
        if(obj.hasOwnProperty(matchJsonFile[i].season))
        {
            obj[matchJsonFile[i].season]++;
        }
        else
        {
            obj[matchJsonFile[i].season]=1;
        }
    }
    //console.log(obj);
    return obj;
} */
// Function 1. Number of matches played per year for all the years in IPL.
// By using reduce().

function matchesPlayedPerYear(matches) {
  return matches.reduce((playedMatches, match) => {
    const year = match.season;

    if (playedMatches[year]) {
      playedMatches[year]++;
    } else {
      playedMatches[year] = 1;
    }
    // console.log(accumulator);
    return playedMatches;
  }, {});
}

// Function 2. Number of matches won of per team per year in IPL.

function winsPerTeamPerYear(matches) {
  return matches.reduce((winningPerTeamPerYear, match) => {
    const winningTeam = match.winner;

    const winningYear = match.season;

    if (winningPerTeamPerYear.hasOwnProperty(winningTeam)) {
      if (winningPerTeamPerYear[winningTeam].hasOwnProperty(winningYear)) {
        winningPerTeamPerYear[winningTeam][winningYear]++;
      } else {
        winningPerTeamPerYear[winningTeam][winningYear] = 1;
      }
    } else {
      winningPerTeamPerYear[winningTeam] = {};
      // if( winningPerTeamPerYear[winningTeam][winningYear] === 0 )
      // winningPerTeamPerYear[winningTeam][winningYear] = 0;
      // else
      winningPerTeamPerYear[winningTeam][winningYear] = 1;
    }
    return winningPerTeamPerYear;
  }, {});
}

// Function3. Extra runs conceded per team in 2016.


function matchesId(matchesFile, Year) {
  // eslint-disable-next-line no-shadow
  const matchId = matchesFile.filter((matchId) => parseInt(matchId.season) === Year);
  const matchID = matchId.map((matches) => matches.id);

  return matchID;
}

function deliveriesMatchId(matchesFile, deliveriesFile, Year) {
  // eslint-disable-next-line camelcase
  const match_Id = matchesId(matchesFile, Year);

  // eslint-disable-next-line no-shadow
  const matchId = deliveriesFile.filter((matchId) => match_Id.includes(matchId.match_id));
  // console.log(matchPerYear.length);
  return matchId;
}

function extraRunPerTeam(matchesFile, deliveriesFile, Year) {
  // console.log(matches.length);

  if (matchesFile.length === 0 || deliveriesFile.length === 0) {
    return 'Data is empty';
  }
  const matchId = deliveriesMatchId(matchesFile, deliveriesFile, Year);

  const deliveryBowling = matchId.reduce((extraRun, deliveryBowling) => {
    if (!extraRun[deliveryBowling.bowling_team]) {
      extraRun[deliveryBowling.bowling_team] = parseInt(deliveryBowling.extra_runs);
    } else {
      extraRun[deliveryBowling.bowling_team] += parseInt(deliveryBowling.extra_runs);
    }

    return extraRun;
  }, {});
  return deliveryBowling;
}


// Function 4.Top 10 economical bowlers in 2015.


function economicalBowler(matchesFile, deliveriesFile, Year) {
  if (matchesFile.length === 0 || deliveriesFile.length === 0) {
    return 'Data is empty';
  }
  const bowler = matchesId(matchesFile, Year);

  const teamBowler = deliveriesFile.filter((everyBowlerPerTeam) => bowler.includes(everyBowlerPerTeam.match_id));

  // console.log(teamBowler.length);

  const economyBowlerOfYear = teamBowler.reduce((deliveryBowler, bowlerEconomy) => {
    if (!deliveryBowler[bowlerEconomy.bowler]) {
      deliveryBowler[bowlerEconomy.bowler] = [0, 0];
    }
    deliveryBowler[bowlerEconomy.bowler][0] += parseInt(bowlerEconomy.total_runs) - (parseInt(bowlerEconomy.legbye_runs) + parseInt(bowlerEconomy.bye_runs));

    if (bowlerEconomy.wide_runs == '0' && bowlerEconomy.noball_runs == '0') { deliveryBowler[bowlerEconomy.bowler][1]++; }


    return deliveryBowler;
  }, {});

  const topEconomy = Object.entries(economyBowlerOfYear);

  const topBowler = topEconomy.reduce((topBowler, delivery) => {
    const over = delivery[1][1] / 6;
    const totalRun = delivery[1][0];

    // eslint-disable-next-line prefer-destructuring
    bowlerName = delivery[0];
    topBowler[bowlerName] = parseFloat((totalRun / over).toFixed(2));

    return topBowler;
  }, {});

  //  console.log(topBowler);


  const result = Object.entries(topBowler);

  result.sort((a, b) => a[1] - b[1]);
  //console.log("res",result);


  return result.slice(0, 10);
}
// Function 5.Top Batsman.


function topBatsman(deliveriesJsonFile) {
  const topBatman = deliveriesJsonFile.reduce((topScore, topRun) => {
    if (!topScore[topRun.batsman]) {
      // eslint-disable-next-line radix
      // eslint-disable-next-line no-param-reassign
      topScore[topRun.batsman] = parseInt(topRun.batsman_runs);
    } else {
      // eslint-disable-next-line no-param-reassign
      topScore[topRun.batsman] += parseInt(topRun.batsman_runs);
    }

    return topScore;
  }, {});

  const result = Object.entries(topBatman);

  result.sort((a, b) => b[1] - a[1]);


  return result.slice(0, 5);
}

module.exports = {
  matchesPlayedPerYear,
  winsPerTeamPerYear,
  extraRunPerTeam,
  economicalBowler,
  topBatsman,
};
