/* eslint-disable max-len */
/* eslint-disable no-undef */
const iplMatches = require('./ipl.js');

// Function 1.
describe('Number of matches played per year for all the years in IPL', () => {
  test(' To check the function is exist or not ', () => {
    expect(iplMatches.matchesPlayedPerYear).toBeDefined();
  });


  test(' To check for empty data:', () => {
    expect(iplMatches.matchesPlayedPerYear([])).toEqual({});
  });

  test('To check for sample data output ', () => {
    const matches = [{ season: 2008 }, { season: 2009 }, { season: 2010 }, { season: 2010 }, { season: 2008 }, { season: 2008 }, { season: 2009 }];
    // eslint-disable-next-line quote-props
    expect(iplMatches.matchesPlayedPerYear(matches)).toEqual({ '2008': 3, '2009': 2, '2010': 2 });
  });
});

// /Function 2.

describe('Number of matches won of per team per year in IPL', () => {
  test(' To check the function is exist or not ', () => {
    expect(iplMatches.winsPerTeamPerYear).toBeDefined();
  });


  test(' To check for empty data:', () => {
    expect(iplMatches.winsPerTeamPerYear([])).toEqual({});
  });

  test('To check for sample data output', () => {
    const matches = [{ winner: 'Sunrisers Hyderabad', season: 2008 }, { winner: 'Rising Pune Supergiant', season: 2008 },
      { winner: 'Kings XI Punjab', season: 2009 }, { winner: 'Sunrisers Hyderabad', season: 2008 },
      { winner: 'Royal Challengers Bangalore', season: 2009 }, { winner: 'Kings XI Punjab', season: 2010 },
      { winner: 'Royal Challengers Bangalore', season: 2010 }];


    // console.log(iplMatches.winsPerTeamPerYear(matches));

    expect(iplMatches.winsPerTeamPerYear(matches)).toEqual(
      {

        'Sunrisers Hyderabad': {
          // eslint-disable-next-line quote-props
          '2008': 2,
        },
        'Rising Pune Supergiant': {
          // eslint-disable-next-line quote-props
          '2008': 1,
        },
        'Royal Challengers Bangalore': {
          // eslint-disable-next-line quote-props
          '2009': 1,
          // eslint-disable-next-line quote-props
          '2010': 1,
        },
        'Kings XI Punjab': {
          // eslint-disable-next-line quote-props
          '2009': 1,
          // eslint-disable-next-line quote-props
          '2010': 1,
        },
      },
    );
  });
});

// Function 3.

describe(' Extra runs conceded per team in 2016.', () => {
  test(' To check the function is exist or not ', () => {
    expect(iplMatches.extraRunPerTeam).toBeDefined();
  });


  test(' To check for empty data:', () => {
    expect(iplMatches.extraRunPerTeam([])).toBe('Data is empty');
  });

  test('To check for sample data output', () => {
    const matches = [
      { id: 1, season: 2008 }, { id: 2, season: 2009 }, { id: 3, season: 2016 },
      { id: 4, season: 2016 }, { id: 5, season: 2010 }, { id: 6, season: 2008 },
    ];

    const deliveries = [
      {
        match_id: 1,
        bowling_team: 'Sunrisers Hyderabad',
        extra_runs: 2,
      },
      {
        match_id: 1,
        bowling_team: 'Sunrisers Hyderabad',
        extra_runs: 0,
      },
      {
        match_id: 2,
        bowling_team: 'Kings XI Punjab',
        extra_runs: 1,
      },
      {
        match_id: 6,
        bowling_team: 'Royal Challenger Bangalore',
        extra_runs: 2,
      },
      {
        match_id: 1,
        bowling_team: 'Sunrisers Hyderabad',
        extra_runs: 2,
      },
      {
        match_id: 2,
        bowling_team: 'Kings XI Punjab',
        extra_runs: 1,
      },
      {
        match_id: 4,
        bowling_team: 'Rising Pune Supergiant',
        extra_runs: 3,
      },
      {
        match_id: 4,
        bowling_team: 'Rising Pune Supergiant',
        extra_runs: 2,
      },
    ];

    //    console.log(iplMatches.extraRunPerTeam(matches,deliveries,2008));

    expect(iplMatches.extraRunPerTeam(matches, deliveries, 2008)).toEqual(
      {
        'Sunrisers Hyderabad': 4,
        'Royal Challenger Bangalore': 2,
      },
    );
  });
});


// Function 4.

describe(' Top 10 economical bowlers in 2015:', () => {
  test(' To check the function is exist or not ', () => {
    expect(iplMatches.economicalBowler).toBeDefined();
  });


  test(' To check for empty data:', () => {
    expect(iplMatches.economicalBowler([])).toBe('Data is empty');
  });

  test('To check for sample data output', () => {
    const matches = [
      { id: 1, season: 2008 }, { id: 2, season: 2009 }, { id: 3, season: 2016 },
      { id: 4, season: 2016 }, { id: 5, season: 2010 }, { id: 6, season: 2008 },
      { id: 7, season: 2016 }, { id: 8, season: 2010 }, { id: 10, season: 2016 },
      { id: 9, season: 2009 }, { id: 3, season: 2016 }, { id: 3, season: 2016 },
    ];

    const deliveries = [
      {
        match_id: 1, bowler: 'Kohli', legbye_runs: 1, total_runs: 13, bye_runs: 0, wide_runs: 0, noball_runs: 0,
      },
      {
        match_id: 6, bowler: 'TS MILL', legbye_runs: 1, total_runs: 19, bye_runs: 0, wide_runs: 0, noball_runs: 0,
      },
      {
        match_id: 1, bowler: 'Pandya', legbye_runs: 1, total_runs: 10, bye_runs: 0, wide_runs: 0, noball_runs: 0,
      },
    ];

    // console.log(iplMatches.economicalBowler(matches, deliveries, 2008));

    expect(iplMatches.economicalBowler(matches, deliveries, 2008)).toEqual(
      // eslint-disable-next-line no-trailing-spaces
      [
        [
          'Pandya',
          54,
        ],
        [
          'Kohli',
          72,
        ],
        [
          'TS MILL',
          108,
        ],
      ],
    );
  });
});


// Function 5.

describe(' Top 5 batsman in whole iplMatches:', () => {
  test(' To check the function is exist or not ', () => {
    expect(iplMatches.topBatsman).toBeDefined();
  });


  test(' To check for empty data:', () => {
    expect(iplMatches.topBatsman([])).toEqual([]);
  });


  test('To check for sample data output', () => {
    const batsman = [{ batsman: 'Virat Kohli', batsman_runs: 18 }, { batsman: 'DA Warner', batsman_runs: 154 },
      { batsman: 'Virat Kohli', batsman_runs: 118 }, { batsman: 'DA Warner', batsman_runs: 15 },
      { batsman: 'Raina', batsman_runs: 54 }, { batsman: 'DA Warner', batsman_runs: 85 },
      { batsman: 'Raina', batsman_runs: 48 }, { batsman: 'Virat Kohli', batsman_runs: 25 },
      { batsman: 'aditya', batsman_runs: 148 }, { batsman: 'Virat Kohli', batsman_runs: 45 },
      { batsman: 'H.pandya', batsman_runs: 38 }, { batsman: 'MS Dhoni', batsman_runs: 45 },
      { batsman: 'KL Rahul', batsman_runs: 48 }, { batsman: 'Rohit Sharma', batsman_runs: 215 }];

    expect(iplMatches.topBatsman(batsman)).toEqual(
      [
        ['DA Warner',
          254,
        ],
        ['Rohit Sharma',
          215,
        ],
        [
          'Virat Kohli',
          206,
        ],
        [
          'aditya',
          148,
        ],
        [
          'Raina',
          102,
        ],

      ],
    );
  });
});
