/* eslint-disable max-len */
/* eslint-disable no-plusplus */
/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */

// Function1 .
function matchesPlayedPerYear() {
  fetch('../output/matchplayed.json')
    .then((response) => (response.json()))
    .then((json) => {
      // console.log(json);
      highChartForIPLMatches(json);
    });
}
// Function3.

function extraRunPerTeam() {
  fetch('../output/year.json')
    .then((response) => response.json())
    .then((json) => {
      // console.log(json);
      highChartForIPLExtraRun(json);
    });
}

// Function4.

function economicalBowler() {
  fetch('../output/topEconomicalBowler.json')
    .then((response) => response.json())
    .then((json) => {
      // console.log(json);
      highChartForIPLEconomic(json);
    });
}

// Function5.


function topBatsman() {
  fetch('../output/topBatsman.json')
    .then((response) => response.json())
    .then((json) => {
      // console.log(json);
      highChartForIPLTopBatsman(json);
    });
}


let IplMatchesYear;

// Function1.  High Charts.

function highChartForIPLMatches(JsonData) {
  IplMatchesYear = Object.keys(JsonData);

  Highcharts.chart('container', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'IPL Matches Played Per Year',
    },
    xAxis: {
      categories: Object.keys(JsonData),
    },
    yAxis: {
      min: 0,
      title: {
        text: 'IPL Matches',
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: ( // theme
            Highcharts.defaultOptions.title.style && Highcharts.defaultOptions.title.style.color
          ) || 'gray',
        },
      },
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: 'Total: {point.stackTotal}',
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
        },
      },
    },
    series: [{
      name: 'Number Of Matches',
      data: Object.values(JsonData),
    }],
  });
}


// Function3.  HighChart

function highChartForIPLExtraRun(JsonData) {
  Highcharts.chart('container', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'IPL Teams of 2016',
    },
    xAxis: {
      categories: Object.keys(JsonData),
    },
    yAxis: {
      min: 0,
      title: {
        text: 'IPL Matches',
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: ( // theme
            Highcharts.defaultOptions.title.style && Highcharts.defaultOptions.title.style.color
          ) || 'gray',
        },
      },
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor:
        // eslint-disable-next-line no-undef
        Highcharts.defaultOptions.legend.backgroundColor || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: 'Total: {point.stackTotal}',
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
        },
      },
    },

    series: [{
      name: 'Extra Runs Of All Team',
      data: Object.values(JsonData),
    },
    ],
  });
}


// Function4. HighChart

function highChartForIPLEconomic(JsonData) {
  const bowler = [];
  const economy = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < JsonData.length; ++i) {
    bowler.push(JsonData[i][0]);
    economy.push(JsonData[i][1]);
  }

  // console.log(bowler);

  Highcharts.chart('container', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'IPL Teams of 2015',
    },
    xAxis: {
      categories: bowler,
    },
    yAxis: {
      min: 0,
      title: {
        text: 'IPL Matches',
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: ( // theme
            Highcharts.defaultOptions.title.style && Highcharts.defaultOptions.title.style.color
          ) || 'gray',
        },
      },
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: 'Total: {point.stackTotal}',
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
        },
      },
    },
    series: [{
      name: 'Top Economical Bowler',
      data: Object.values(JsonData),
    }],
  });
}

// Function5 .HighChart

function highChartForIPLTopBatsman(JsonData) {
  const batsman = [];
  const highestScore = [];
  for (let i = 0; i < JsonData.length; i++) {
    batsman.push(JsonData[i][0]);
    highestScore.push(JsonData[i][1]);
  }
  Highcharts.chart('container', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'IPL Matches',
    },
    xAxis: {
      categories: batsman,
    },
    yAxis: {
      min: 0,
      title: {
        text: 'IPL Matches',
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: ( // theme
            Highcharts.defaultOptions.title.style && Highcharts.defaultOptions.title.style.color
          ) || 'gray',
        },
      },
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: 'Total: {point.stackTotal}',
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
        },
      },
    },
    series: [{
      name: ' Top HighestScorer Batsman',
      data: Object.values(JsonData),
    }],
  });
}

// Function2. HighChart


function winsPerTeamPerYear() {
  fetch('../output/winningPerTeamPerYear.json')
    .then((response) => response.json())
    .then((json) => {
            highChartForIPLWinningTeamPerYear(json);
    });
}


// eslint-disable-next-line no-shadow
function numberOfIPLMatches(numberOfIPLMatchesPerTeam, IplMatchesYear) {
  const numberOfMatchesWinning = IplMatchesYear.reduce((matchPlayedPerYear, year) => {
    const numberOfwinningMatchesInAllYear = numberOfIPLMatchesPerTeam.reduce((numberOfMatchesPlayedPerYear, matches) => {
      if (Object.keys(numberOfMatchesPlayedPerYear).length !== 2) {
        // eslint-disable-next-line no-param-reassign
        numberOfMatchesPlayedPerYear.name = year;
        // eslint-disable-next-line no-param-reassign
        numberOfMatchesPlayedPerYear.data = [];
      }

      // eslint-disable-next-line no-prototype-builtins
      if (matches.hasOwnProperty(year)) {
        numberOfMatchesPlayedPerYear.data.push(matches[year]);
      } else {
        numberOfMatchesPlayedPerYear.data.push(0);
      }
      return numberOfMatchesPlayedPerYear;
    }, {});
    matchPlayedPerYear.push(numberOfwinningMatchesInAllYear);
    return matchPlayedPerYear;
  }, []);

  return numberOfMatchesWinning;
}


function highChartForIPLWinningTeamPerYear(JsonData) {
  Highcharts.chart('container', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'IPL Team',
    },
    subtitle: {
      text: 'Number of winning matches',
    },
    xAxis: {
      categories: Object.keys(JsonData),
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: 'IPL Matches',
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
        + '<td style="padding:0"><b>{point.y} matches</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true,
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0,
      },
    },
    // series:[{
    //    name:'IPL Matches',
    //    data:Object.values(JsonData)
    // }]
    series: numberOfIPLMatches(Object.values(JsonData), IplMatchesYear),
  });
}


topBatsman();
winsPerTeamPerYear();
economicalBowler();
matchesPlayedPerYear();
extraRunPerTeam();
